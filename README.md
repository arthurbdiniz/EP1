# EP1 - OO (UnB - Gama)

Este projeto consiste em um programa em C++ capaz de aplicar filtros em imagens de formato `.ppm`.


## Profº Renato

## Aluno: Arthur Barbosa Diniz - 15/0118457

## Neste arquivo deve conter as instruções de execução e descrição do projeto.

Para compilar e executar, siga as seguintes instruções:

    Abra o terminal
    Entre no diretório do projeto: $ cd EP1/
    Limpe os arquivos .o: *$ make clean**
    Compile o programa: $ make
    Execute: $ make run
    Escreva o nome da imagem: ex $xmas
    Escreva o nome da imagem de saida com filtro que sera criada: ex $xmas_polarizado
    
Arquivos, pastas e imagens

    Os arquivos das imagens devem estar contidos na pasta EP1/doc/
    Após o processamento da imagens ela sera lida e copiada para a pasta EP1/filtros/
    Apos dar o $ make run coloque o nome da imagem a ser aplicada o filtro sem sua extenção ".ppm" no final
    Ex: xmas










