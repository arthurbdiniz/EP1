#ifndef FILTRO_PRETO_BRANCO_HPP
#define FILTRO_PRETO_BRANCO_HPP

#include <iostream>
#include "imagem.hpp"
#include <string>
 
 
class FiltroPretoBranco : public Imagem{

	private:
	//Atributos	

	public:
 		FiltroPretoBranco(); //Construtor
 		~FiltroPretoBranco(); //Destrutor

 		void gravaNovaImagem(ofstream &arq_saida); 
 		
};


#endif