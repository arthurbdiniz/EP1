#ifndef IMAGEM_HPP
#define IMAGEM_HPP

#include <iostream>
#include <string>
#include <cstring>
#include <fstream>
#include <cstdio>
#include <cstdlib>

 
using namespace std;


 
 
class Imagem{
 
private:
	//Atributos
 	string nome_arquivo;
 	string comentario;
 	string tipo;
 	int altura;
 	int largura;
 	int intensidade;
 	
 	

public:


	ofstream arq_saida;
 	
	unsigned char **r;
 	unsigned char **g;
 	unsigned char **b;
 	 

 	//Metodos
 	Imagem();
 	~Imagem();
 
 	string getNome(); 
 	void setNome(string nome_arquivo);

 	int getAltura(); 
 	void setAltura(int altura);

 	int getLargura(); 
 	void setLargura(int largura);

 	int getIntensidade();
 	void setIntensidade(int intensidade);

 	string getTipo();
 	void setTipo(string tipo);

 	string getComentario(); 
 	void setComentario(string comentario);
 	
 	void gravaMatriz(ofstream &arq_saida); 
 	virtual void gravaNovaImagem(ofstream &arq_saida); 


 	
 	
 	
 	
};

#endif
