#ifndef FILTRO_HPP
#define FILTRO_HPP
 
using namespace std;
#include <string>
//#include "imagem.hpp"
 
 
class Filtro{

	private:
	//Atributos
	//Imagem *imagem;
	int largura;
	int altura;
	int intensidade;
	string comentario;
	string tipo;
	
		

	public:
		Filtro();

 		Filtro(string tipo, string comentario, int largura, int altura, int intensidade);//Construtor 
 		//Imagem * imagem
 		~Filtro(); //Destrutor


		int getAltura(); 

 		int getLargura(); 

 		int getIntensidade();

 		string getTipo();

 		string getComentario();


 		
 	


};


#endif