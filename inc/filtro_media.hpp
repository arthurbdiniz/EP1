#ifndef FILTRO_MEDIA_HPP
#define FILTRO_MEDIA_HPP
 
#include <iostream>
#include "imagem.hpp"
#include <string>
 
 
class FiltroMedia : public Imagem{

	private:
	//Atributos	

	public:
 		FiltroMedia(); //Construtor
 		~FiltroMedia(); //Destrutor
 		
 		void gravaNovaImagem(ofstream &arq_saida);

};


#endif