#include <iostream>
#include <string>
#include "filtro_preto_branco.hpp"


 
FiltroPretoBranco::FiltroPretoBranco(){ //Construtor
 
 
}
 
FiltroPretoBranco::~FiltroPretoBranco(){//Destrutor
 
 
}


void FiltroPretoBranco::gravaNovaImagem(ofstream &arq_saida){

    int escala_cinza = 0;
    int largura;
    int altura;
    int intensidade;

    largura = getLargura();
    altura = getAltura();
    intensidade = getIntensidade();
     
    

    for(int i = 0; i < altura; i++){
    	for(int j = 0; j < largura; j++){
           	
           	escala_cinza = (0.299 * r[i][j]) + (0.587 * g[i][j]) + (0.144 * b[i][j]);

            if(escala_cinza > intensidade){
               escala_cinza = intensidade; 

            }

                r[i][j] = (unsigned char)escala_cinza;
                g[i][j] = (unsigned char)escala_cinza;
                b[i][j] = (unsigned char)escala_cinza;

                arq_saida.write((char*)&r[i][j], sizeof(unsigned char));
                arq_saida.write((char*)&g[i][j], sizeof(unsigned char));
                arq_saida.write((char*)&b[i][j], sizeof(unsigned char));

    	}
	}
 }
