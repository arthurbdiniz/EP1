#include <iostream>
#include <string>
#include "filtro_polarizado.hpp"

 
FiltroPolarizado::FiltroPolarizado(){ //Construtor
 
 
}
 
FiltroPolarizado::~FiltroPolarizado(){//Destrutor
 
 
}

void FiltroPolarizado::gravaNovaImagem(ofstream &arq_saida){

	int largura;
  int altura;
  int cor_max;
  
  largura = getLargura();
  altura = getAltura();
  cor_max = getIntensidade();

     
	for(int i=0; i < altura ; i++){
 
      	for(int j=0; j < largura ; j++){
        	
      		

      		if((int)r[i][j] < cor_max/2){
               	r[i][j] = 0;
               	
           	}else{
               	r[i][j] = (unsigned char)cor_max;
               	
           	}

           	if((int)g[i][j] < cor_max/2){
               	g[i][j] = 0;
               	

           	}else{
               	g[i][j] = (unsigned char)cor_max;
               	
           	}

           	if((int)b[i][j] < cor_max/2){
               	b[i][j] = 0;
               	
           	}else{
               	b[i][j] = (unsigned char)cor_max;
               	
           	}
           	
           	arq_saida.write((char*)&r[i][j], sizeof(unsigned char));
            arq_saida.write((char*)&g[i][j], sizeof(unsigned char));
            arq_saida.write((char*)&b[i][j], sizeof(unsigned char));

           	
        }
    }
}
