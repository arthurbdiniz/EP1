#include <iostream>
#include <string>
#include "filtro_negativo.hpp"
#include "imagem.hpp"

 
using namespace std;

FiltroNegativo::FiltroNegativo(){ //Construtor
 
  
   
}
 
FiltroNegativo::~FiltroNegativo(){//Destrutor
 
 
}


void FiltroNegativo::gravaNovaImagem(ofstream &arq_saida){

   
    int altura;
    int largura;
    int intensidade;


    altura = getAltura();
    largura = getLargura();
    intensidade = getIntensidade();
   

    for(int i=0; i < altura ; i++){
        for(int j=0; j < largura ; j++){
        
            r[i][j] = intensidade - (int)r[i][j]; 
            g[i][j] = intensidade - (int)g[i][j]; 
            b[i][j] = intensidade - (int)b[i][j];   


            arq_saida.write((char*)&r[i][j], sizeof(unsigned char));
            arq_saida.write((char*)&g[i][j], sizeof(unsigned char));
            arq_saida.write((char*)&b[i][j], sizeof(unsigned char));

        }
    }
}
