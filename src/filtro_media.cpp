#include <iostream>
#include <string>
#include "filtro_media.hpp"



using namespace std;
 
FiltroMedia::FiltroMedia(){ //Construtor
 
 
}
 
FiltroMedia::~FiltroMedia(){//Destrutor
 
 
}

void FiltroMedia::gravaNovaImagem(ofstream &arq_saida){

	
	int largura;
	int altura;
	int tamanho_filtro;
	int R_aux = 0;
	int G_aux = 0;
	int B_aux = 0;
	int contador = 0;


	largura = getLargura();
	altura = getAltura();


	cout << "Tamanho do Filtro, ex 3, 5, 7: " << endl;
	
	do{

		cin >> tamanho_filtro;
		if(tamanho_filtro < 0){
			cout << "Insira valor positivo"<<endl;
		}

	}while(tamanho_filtro < 0);


	tamanho_filtro = tamanho_filtro/2;

	
	for(int x=0; x<altura; x++){
		for(int y=0; y<largura; y++){
			
			for(int a=-tamanho_filtro; a<=tamanho_filtro; a++){
				for(int c=-tamanho_filtro; c<=tamanho_filtro; c++){

					if((x+a) >= 0 && (a+x) < altura){
						if((y+c)>=0 && (y+c) < largura){
							R_aux += (int)r[x+a][y+c];
							G_aux += (int)g[x+a][y+c];
							B_aux += (int)b[x+a][y+c];
							contador++;
						}
					}
				}
			}

			R_aux = R_aux/contador;
			G_aux = G_aux/contador;
			B_aux = B_aux/contador;

			r[x][y] = (unsigned char)R_aux;
			g[x][y] = (unsigned char)G_aux;
			b[x][y] = (unsigned char)B_aux;

			arq_saida.write((char*)&r[x][y], sizeof(unsigned char));
            arq_saida.write((char*)&g[x][y], sizeof(unsigned char));
            arq_saida.write((char*)&b[x][y], sizeof(unsigned char));


			R_aux = 0;
			G_aux = 0;
			B_aux = 0;
			contador = 0; 

		}
	}
}
