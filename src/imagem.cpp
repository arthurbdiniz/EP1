#include "imagem.hpp"
#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <cstdlib>
#include <sstream>

 

using namespace std;


Imagem::Imagem(){ //Construtor

	
  setLargura(0);
 	setAltura(0);
	setIntensidade(0);
	setTipo("P6");
	setNome("");

	

  r = new unsigned char*[altura];
  for(int i = 0; i < altura; i++)
  r[i] = new unsigned char[largura];

  g = new unsigned char*[altura];
  for(int i = 0; i < altura; i++)
  g[i] = new unsigned char[largura];

  b = new unsigned char*[altura];
  for(int i = 0; i < altura; i++)
  b[i] = new unsigned char[largura];
 
}
 

Imagem::~Imagem(){//Destrutor
 
}
 
void Imagem::gravaMatriz(ofstream &arq_saida){

  //Declaracoes
	int largura;
	int altura;
	int intensidade;
	string comentario;
	string tipo;
  string nome_saida;
  char opcao;

		
	ifstream arq_entrada;

  cout << "\n\nNome do arquivo de entrada: ";
  cin >>  nome_arquivo;

	arq_entrada.open(("./doc/"+nome_arquivo+".ppm").c_str(), ios::binary | ios::in);
	
	if(!arq_entrada.is_open()){ // verifica se o arquivo foi aberto corretamente
                cout << "Erro ao abrir o arquivo de entrada\nTente novamente..." << endl;
                arq_entrada.close();
               	exit(1);
	}

  cout << "Arquivo de entrada encontrado..." << endl;

  cout << "\nNome do Arquivo de Saida: ";
  cin >> nome_saida;


	arq_entrada >> tipo;

	if(tipo != "P6"){
        
        cout << "Formato invalido de arquivo" << endl;
 		arq_entrada.close();
        exit(1);

  }
  setTipo(tipo);

   char c;
   std::streampos aux;

    do{     
      aux = arq_entrada.tellg();     
      arq_entrada >> c;    
      if(c == '#'){       
        getline(arq_entrada,comentario);       
      }else{             
        arq_entrada.seekg(aux);         
      }  

    }while(c == '#');
   
	
  
  arq_entrada >> largura; 
  arq_entrada >> altura;
  arq_entrada >> intensidade;    
	

	setComentario(comentario);
  setAltura(altura);
  setLargura(largura);
  setIntensidade(intensidade);


  cout << "\nARQUIVO" << endl;
  cout << "Nome: " << getNome() << endl;
  cout << "Tipo: " << getTipo() << endl;
  cout << "Comentario: " << getComentario() << endl;
  cout << "Altura: " << getAltura() << endl;
  cout << "Largura: " << getLargura() << endl;
  cout << "Intensidade: " << getIntensidade() << endl;


  arq_entrada.seekg(1, ios_base::cur);
	
	arq_saida.open(("./filtros/"+nome_saida+".ppm").c_str(), ios::binary | ios::app | ios::out);

  arq_saida << getTipo() << endl;
  //arq_saida << getComentario() << endl;
  arq_saida << getLargura() << endl;
  arq_saida << getAltura() << endl;
  arq_saida << getIntensidade() << endl;
    

  r = new unsigned char*[altura];
  for(int i = 0; i < altura; i++)
  r[i] = new unsigned char[largura];

  g = new unsigned char*[altura];
  for(int i = 0; i < altura; i++)
  g[i] = new unsigned char[largura];

  b = new unsigned char*[altura];
  for(int i = 0; i < altura; i++)
  b[i] = new unsigned char[largura];


	for(int i=0; i<altura; i++){
 		for(int j=0; j<largura; j++){
 	
 			arq_entrada.read((char*)&r[i][j], 1);
      arq_entrada.read((char*)&g[i][j], 1);
      arq_entrada.read((char*)&b[i][j], 1);
 			
 
 	    }
	}

  gravaNovaImagem(arq_saida);

  
	arq_entrada.close();
  arq_saida.close();


  cout << "Arquivo gravado com sucesso, deseja gravar outro arquivo?(S/N)";
  cin >> opcao;

  if(opcao == 'S' || opcao == 's'){
    gravaMatriz(arq_saida);

  }

  cout << "Volte Sempre!\n" << endl;


}

void Imagem::gravaNovaImagem(ofstream &arq_saida){

  for(int i=0; i < altura ; i++){
    for(int j=0; j < largura ; j++){
        
        arq_saida.write((char*)&r[i][j], sizeof(unsigned char));
        arq_saida.write((char*)&g[i][j], sizeof(unsigned char));
        arq_saida.write((char*)&b[i][j], sizeof(unsigned char));
        
    }
  }

}


string Imagem::getNome(){
 	
	return nome_arquivo;
}
 
void Imagem::setNome(string nome_arquivo){
 	
 	this->nome_arquivo = nome_arquivo;
}


int Imagem::getAltura(){
 	
	return altura;
}

void Imagem::setAltura(int altura){
 	
 	this->altura = altura;
}


int Imagem::getLargura(){
 	
	return largura;
}
 
void Imagem::setLargura(int largura){
 	
 	this->largura = largura;
}
 

int Imagem::getIntensidade(){
	
	return intensidade;
}
void Imagem::setIntensidade(int intensidade){

	this->intensidade = intensidade;
}
 
string Imagem::getTipo(){

	return tipo;
}

void Imagem::setTipo(string tipo){

	this->tipo = tipo;
}


string Imagem::getComentario(){

 	return comentario;
}
 
void Imagem::setComentario(string comentario){

	this->comentario = comentario;
}


