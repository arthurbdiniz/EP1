#include <iostream>
#include <fstream>
#include <string>
#include <cstring>

#include "imagem.hpp"
#include "filtro_negativo.hpp"
#include "filtro_preto_branco.hpp"
#include "filtro_polarizado.hpp"
#include "filtro_media.hpp"
 

using namespace std;

int main(){
 

 	int opcao;


 	//MENU 
 	cout << "\nMENU FILTRO\n" << endl;
 	cout << "1-Normal\n2-Negativo\n3-Preto e Branco\n4-Polarizado\n5-Media" << endl;
 	cin >> opcao;
	

	ofstream arq_saida; // Ponteido para arquivo de saida

 	Imagem *imagem =  new Imagem; //Criando o Objeto imagem
	FiltroNegativo *filtro_negativo = new FiltroNegativo;	//Criando o Objeto Filtro Negativo
	FiltroPretoBranco *filtro_preto_branco = new FiltroPretoBranco; //Criando o Objeto Filtro Preto e Branco
	FiltroPolarizado *filtro_polarizado = new FiltroPolarizado; //Criando o Objeto Filtro Polarizado
	FiltroMedia *filtro_media = new FiltroMedia; //Criando o Objeto Filtro Média
 

 	switch(opcao){

 		case 1: //Normal
 			imagem->gravaMatriz(arq_saida);break;

 		case 2: //Negativo
 			filtro_negativo->gravaMatriz(arq_saida);break;
 		
 		case 3: //Preto e Branco
 			filtro_preto_branco->gravaMatriz(arq_saida);break;
 		

 		case 4: //Polarizado
 			filtro_polarizado->gravaMatriz(arq_saida);break;


 		case 5: //Media
 			filtro_media->gravaMatriz(arq_saida);break;

 		
 		default:

 		break;

 	}
 	 
 return 0;

}

